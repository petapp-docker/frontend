export interface IPicture{
    id: number;
    title?:string;
    path:string;
    description?:string;
    author?:string;
    source?:string;
    alt:string;
    class?:string;
}