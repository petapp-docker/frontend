import { ERole } from "./ERole";

export interface IUser{
    id:number;
    username:string;
    password?:string;
    role?:ERole;
}