# FEAngularPetProject

**The app to see your pet data.**

## Description
FRONT END IN ANGULAR
- The second bigger practical project during SDA JavaRemoteee27. Project creation time 16.12.23-21.01.24.
- This is the front end side of the project, designed in Angular.
- The App is at the development phase right now, the idea is to design back-end in Spring Boot and front-end in Angular.
- Uses [Angular CLI](https://github.com/angular/angular-cli) version 17.0.7

### Feature list

**NEW:**
- Add picture of your pet (user / admin)
- See Pet clinics (names, addresses: city + street, phones).

**When Spring BE & MySQL is running, then in Angular FE you can:**
- See home, about and contacts page (w/o login)
- Role based login and rights (admin, user)
- Admin role can manage all pets (see, add + connect to user, update, delete)
- User role can see and manage their own pets
- Register a new user
- Login to see:
  - your pets info from database,
  - add new pets,
  - edit your pets data,
  - delete your pets data
- JWT OAuth2 token-based login

### Current state
- Angular page has header menu for routing
- Pages have basic design
- Home page card components are reusable, created from scratch (*ngFor, property binding)
- About page card components are reusable, created w Angular Material (*ngFor, property binding)
- Contact page has a contact form //TODO:: implement sending e-mail
- Pet clinics page (see clinics and their info). No login needed.
- Pets page needs login
- You may register a new user
- REST API used to fetch, delete and add pets data (from MySql DB through BE)
- Frontend login: users credentials are saved in DB and
- while login saved in local storage (JWT OAuth2 token based login) //TODO:: tokens to HttpOnly cookie
- Two dummy (demo)users are initially created while running the app:
- ROLE_USER login:
  - user1 / user
- ROLE_ADMIN login:
  - admin1 / admin
- Login: http://localhost:4200/pets
- Credentials are encrypted in DB
- See `./PetProject/src/main/java/com/example/petproject/PetProjectApplication.java` for the dummy (demo)users
- See `./PetProject/src/main/java/com/example/petproject/config/SecurityConfig.java` for the user roles / rights
- Users, admins can access and manipulate their own pets data (CRUD).
- Admin role can manage all pets (see, add + connect to user, update, delete). CRUD
- App has basic mobile-views (customized for different types of mobiles)


![Angular3_20.01.24.png](..%2FImages%2FAngular3_20.01.24.png)

![Angular_about_20.01.24.png](..%2FImages%2FAngular_about_20.01.24.png)

![Angular_contact_20.01.24.png](..%2FImages%2FAngular_contact_20.01.24.png)

![Angular_login_3_6.01.24.png](..%2FImages%2FAngular_login_3_6.01.24.png)

![Angular_register_3_26.01.24.png](..%2FImages%2FAngular_register_3_26.01.24.png)

![Angular_register_4_26.01.24.png](..%2FImages%2FAngular_register_4_26.01.24.png)

![user_own_space_20.01.24.png](..%2FImages%2Fuser_own_space_20.01.24.png)

![Angular_tokens_localstorage_24.01.24.png](..%2FImages%2FAngular_tokens_localstorage_24.01.24.png)

![select_picture_22.02.24.png](..%2FImages%2Fselect_picture_22.02.24.png)

![petpictures_22.02.24.png](..%2FImages%2Fpetpictures_22.02.24.png)

![Angular_pets_6_31.01.24.png](..%2FImages%2FAngular_pets_6_31.01.24.png)

![Angular_edit_pets_3_2.02.24.png](..%2FImages%2FAngular_edit_pets_3_2.02.24.png)

![Angular_pets_5_31.01.24.png](..%2FImages%2FAngular_pets_5_31.01.24.png)

![Angular_pets_6_31.01.24.png](..%2FImages%2FAngular_pets_6_31.01.24.png)

![Angular_edit_pets_3_2.02.24.png](..%2FImages%2FAngular_edit_pets_3_2.02.24.png)

![Angular_pets_mobile_GS20_1_31.01.24.png](..%2FImages%2FAngular_pets_mobile_GS20_1_31.01.24.png)

![Angular_pets_mobile_SG8+_20.01.24.png](..%2FImages%2FAngular_pets_mobile_SG8%2B_20.01.24.png)

**Admin view**

![admin_page1_31.01.24.png](..%2FImages%2Fadmin_page1_31.01.24.png)

![admin_page3_03.02.24.png](Images%2Fadmin_page3_03.02.24.png)

![admin_pet_edit_page_3.2.24.png](Images%2Fadmin_pet_edit_page_3.2.24.png)

**Error message when trying to perform non-authorized actions:**

![Angular_role_based_authorization_06.01.24.png](..%2FImages%2FAngular_role_based_authorization_06.01.24.png)

![Angular_role_based_authorization_deleting_06.01.24.png](..%2FImages%2FAngular_role_based_authorization_deleting_06.01.24.png)

![Angular_role_based_authorization_editing_06.01.24.png](..%2FImages%2FAngular_role_based_authorization_editing_06.01.24.png)

![Angular_register_errors_1_1.01.24.png](..%2FImages%2FAngular_register_errors_1_1.01.24.png)

![Angular_pets_5_20.01.24.png](..%2FImages%2FAngular_pets_5_20.01.24.png)

## Prerequisites to run the code
- You need [Angular](https://github.com/angular/angular-cli) version 17.0.7 to run this Angular FE in your computer. (Previous than 17 and later than 17 builds might not be compatible)
- [Node.js 18.18.2 (LTS)](https://nodejs.org/en/blog/release/v18.18.2). The program has been created with Node.js 18.18.2 (LTS). (By Angular 17 doc it might not be compatible with Node.js 20.10.0 LTS version (yet)).

## Launch procedure of the Angular project

**Install Node.js 18.18.2 and Angular 17.0.7**

- Install [Node.js 18.18.2 (LTS)](https://nodejs.org/en/blog/release/v18.18.2). 
- Run `nmp -v` in your command line after installation to check that **npm** is installed
- Install Angular CLI (on Windows command-line: `npm install -g @angular/cli@17.0.7`)

**Git clone and run the Angular project**

- `git clone https://gitlab.com/java-remote-ee27-course/petapp.git`
- open FEAngularPetProject directory in your favourite IDE or editor (e.g. IntelliJ or VSCode)
- run `ng serve` in IDE / editor command-line tool
- follow the instructions from command-line to initialize the new project
- run `ng serve` once more and navigate to `http://localhost:4200/`.

**Learn Angular**
- [Angular basics and tutorials](https://angular.io/guide/understanding-angular-overview)


## Technologies used 
- Created with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.7 on Node.js 18.18.2 (LTS)
- Angular Material design
- Bootstrap design

## Authors
Katlin Kalde and ...

## Credits
(Demo photos used in Angular home page carousel: Pixabay)

