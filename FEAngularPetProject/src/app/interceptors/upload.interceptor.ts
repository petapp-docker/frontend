import { HttpEvent, HttpHandler, HttpInterceptor, HttpInterceptorFn, HttpRequest } from '@angular/common/http';
import { BrowserStorageService } from '../services/storage.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class UploadInterceptor implements HttpInterceptor {
  constructor(private storage: BrowserStorageService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const isApiUrl = request.url.startsWith('http://localhost:8080/api/api/files'); //TODO:: change URL later if needed!
    if (isApiUrl) {
      const headers: any = {
        'Content-Type': 'multi-part/formdata',
        'X-Requested-With': 'XMLHttpRequest',
      }

      //next call gets the tokens from localStorage 
      const token = this.storage.get('token');
      if (token) {
        headers.Authorization = 'Bearer ' + token;
      }

      request = request.clone({
        setHeaders: headers,
      });
    }
    return next.handle(request);
  }
}


