import { mergeApplicationConfig, ApplicationConfig } from '@angular/core';
import { provideServerRendering } from '@angular/platform-server';
import { appConfig } from './app.config';
import { BrowserStorageService } from './services/storage.service';
import { BrowserStorageServerService } from './services/browser-storage-server.service';

/**
 * See: src/app/services/storage.service.ts and
 * https://javascript.plainenglish.io/how-to-handle-browser-storage-in-angular-ssr-d24391698d21 
 */
const serverConfig: ApplicationConfig = {
  providers: [
    provideServerRendering(),
    {
      provide: BrowserStorageService,
      useClass: BrowserStorageServerService,
    }
  ]
};

export const config = mergeApplicationConfig(appConfig, serverConfig);
