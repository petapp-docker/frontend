import { Inject, Injectable, InjectionToken } from '@angular/core';

/** 
 * Browser storage to access the browser storage when needed.
 * See the complete logic from
 * https://angular.dev/guide/di/di-in-action#custom-providers-with-inject
 * https://javascript.plainenglish.io/how-to-handle-browser-storage-in-angular-ssr-d24391698d21 
 * See also: app.config.server.ts
 */

export const BROWSER_STORAGE = new InjectionToken<Storage>('Browser Storage', {
  providedIn: 'root',
  factory: () => localStorage
});

@Injectable({ providedIn: 'root' })
export class BrowserStorageService {
  constructor(@Inject(BROWSER_STORAGE) public storage: Storage) {}
  get(key: string) {
    return this.storage.getItem(key);
  }
  set(key: string, value: string) {
    this.storage.setItem(key, value);
  }

  remove(key: string) {
    this.storage.removeItem(key);
  }

  clear() {
    this.storage.clear();
  }
}