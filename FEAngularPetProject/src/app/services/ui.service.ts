import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private showAddPet: boolean = false;
  private subject = new Subject<any>();


  constructor() { }

  //toggling button:
  toggleAddPet(): void{
    this.showAddPet = !this.showAddPet;
    this.subject.next(this.showAddPet);
  }

  //and when you are toggling (toggleAddClass)
  //then onToggle is called
  onToggle(): Observable<any>{
    return this.subject.asObservable();

  }
}
