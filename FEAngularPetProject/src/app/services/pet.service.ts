import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { IPet } from '../../../models/IPet';
import { environment } from '../../environments/environment';


@Injectable({ providedIn: 'root' })
export class PetService {

  //url comes from environment (environment.development) file,
  //header options come from auth interceptors, if url is ApiUrl (isApiUrl)
  url = `${environment.url}`;

  constructor(
    private httpClient: HttpClient) {}


  getPets(urlEnd:string): Observable<any>  {
    const url = `${this.url}/${urlEnd}`;
    return this.httpClient.get<any>(url); 
  }

  getPet(urlEnd:string, id: number): Observable<IPet> {
    const url = `${this.url}/${urlEnd}/${id}`;
    return this.httpClient.get<IPet>(url);
  }

  addPet(urlEnd:string, pet: IPet, imageId: number): Observable<IPet>{
    const url = `${this.url}/${urlEnd}`;
    const requestBody = {pet: pet, imageId: imageId};
    return this.httpClient.post<IPet>(url, requestBody); 
  } 

  deletePet(urlEnd:string, pet: IPet): Observable<IPet>{
    const url = `${this.url}/${urlEnd}/${pet.id}`;
    return this.httpClient.delete<IPet>(url); 
  }

  updatePet(urlEnd:string, pet: IPet): Observable<IPet>{
    const url = `${this.url}/${urlEnd}/${pet.id}`;
    return this.httpClient.put<IPet>(url, pet);
  }
}



