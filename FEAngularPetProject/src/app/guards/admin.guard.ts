import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { inject } from '@angular/core';
import { AuthService } from '../services/auth.service';

//See also app.routes.ts, it is used to show forbidden page instead of letting restricted users to admin page by admin url
export const adminGuard: CanActivateFn = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree => {
  const authService = inject(AuthService); 
  const router = inject(Router);

  
  if (authService.isAdmin()) {
    return true;
  } else {
   
      router.navigate(['/forbidden'], { queryParams: { returnUrl: state.url } });
      return router.createUrlTree(['/forbidden']); // Redirect to home page   

  }       

};
