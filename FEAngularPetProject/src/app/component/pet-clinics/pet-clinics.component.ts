import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Component } from '@angular/core';
import { IPetClinic } from '../../../../models/IPetClinic';
import { PetClinicService } from '../../services/pet-clinic.service';


@Component({
  selector: 'app-pet-clinics',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './pet-clinics.component.html',
  styleUrl: './pet-clinics.component.css'
})
export class PetClinicsComponent {

  petClinics: Array<IPetClinic> = [];
  //petClinicAddress: Array<IAddress> = [];
  petClinicsUrl: string = 'petclinics';


  constructor(private petClinicService: PetClinicService) {

  }

  ngOnInit(): void {
    this.getPetClinics();
  }

  getPetClinics() {
    this.petClinicService.getPetClinics(this.petClinicsUrl)
      .subscribe(value => {
        this.petClinics = value;
      });

  }

  //Change the column span depending of how many phones
  changeColspan(object: Object[]): number {
    if (object.length < 2) {
      return 2;
    }
    else if (object.length < 3) {
      return 1;
    }
    return 1;
  }

  //Explanation - slice takes first 2 elements with *ngFor (and does not display others)
  // [attr.colspan]="changeColspan(petClinic.petClinicPhones!) - uses changeColspan method to change the column span in:
  // <td *ngFor ="let phone of petClinic.petClinicPhones! | slice:0:2;" 
  //                  [attr.colspan]="changeColspan(petClinic.petClinicPhones!)>
}
