import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { CarouselComponent } from '../carousel/carousel.component';
import { IPicture } from '../../../../models/IPicture';
import { CARDHEADING, CARDPICTUREFIRST, CARDPICTURESECOND, CARDPICTURETHIRD, CAROUSELOPTIONS } from '../../card-content';
import { IHeadings } from '../../../../models/IHeadings';
import { ICarouselOptions } from '../../../../models/ICarouselOptions';

@Component({
    selector: 'app-home-main',
    standalone: true,
    templateUrl: './home-main.component.html',
    styleUrl: './home-main.component.css',
    imports: [CarouselComponent, CommonModule]
})
export class HomeMainComponent {

    // CARDPICTURE is the content from '../../pictures-content'
    // all the data is in card-content.ts file:
    picturesFirst : IPicture[] = CARDPICTUREFIRST;
    picturesSecond : IPicture[] = CARDPICTURESECOND;
    picturesThird : IPicture[] = CARDPICTURETHIRD;
    headingTextFirst: IHeadings = CARDHEADING[0];
    headingTextSecond: IHeadings = CARDHEADING[1];
    headingTextThird: IHeadings = CARDHEADING[2];
    carouselButtonFirst: ICarouselOptions = CAROUSELOPTIONS[0];
    carouselButtonSecond: ICarouselOptions = CAROUSELOPTIONS[1];
    carouselButtonThird: ICarouselOptions = CAROUSELOPTIONS[2];
}
