import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IPetClinic } from '../../../../models/IPetClinic';
import { ActivatedRoute } from '@angular/router';
import { PetClinicService } from '../../services/pet-clinic.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pet-clinic',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './pet-clinic.component.html',
  styleUrl: './pet-clinic.component.css'
})
export class PetClinicComponent {
  errorMessage: any;
  petClinic!: IPetClinic;
  petClinicsUrl: string = 'petclinics';

  constructor(
    private route:ActivatedRoute,
    private petClinicService:PetClinicService,
    private location: Location
  ){} 

  ngOnInit(): void {
    this.getPetClinic();
  }

  getPetClinic(): void{
    const id = +this.route.snapshot.paramMap.get('id')!;
    this.petClinicService.getPetClinic(this.petClinicsUrl, id)
        .subscribe(pc => this.petClinic = pc);
  }

  goBack():void {
    this.location.back();
  }

}
