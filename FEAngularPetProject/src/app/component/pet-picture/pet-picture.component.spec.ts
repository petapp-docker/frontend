import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetPictureComponent } from './pet-picture.component';

describe('PetPictureComponent', () => {
  let component: PetPictureComponent;
  let fixture: ComponentFixture<PetPictureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PetPictureComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PetPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
