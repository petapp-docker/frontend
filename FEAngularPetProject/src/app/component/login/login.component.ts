import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { NgForm } from '@angular/forms';
import { error } from 'console';



// Login
@Component({
    selector: 'app-login',
    standalone: true,
    templateUrl: './login.component.html',
    styleUrl: './login.component.css',
    imports: [FormsModule, CommonModule]
})


export class LoginComponent {
  message: string ='';
  errorMessage:string = '';
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService) {
      this.message = this.router.getCurrentNavigation()!.extras.state?.['message'];
    }
   

  onSubmit(loginForm: NgForm): void {
    this.authService.login(loginForm.value.username, loginForm.value.password)
      .subscribe({
        next: (value) => {
          this.errorMessage = this.authService.message;
        },
        error: (error) => {
          this.errorMessage = 'Could not login!';
          console.log(error);
        }
      });
    this.message = '';
  }

}
