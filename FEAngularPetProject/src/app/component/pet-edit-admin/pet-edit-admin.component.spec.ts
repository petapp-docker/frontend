import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetEditAdminComponent } from './pet-edit-admin.component';

describe('PetEditAdminComponent', () => {
  let component: PetEditAdminComponent;
  let fixture: ComponentFixture<PetEditAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PetEditAdminComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PetEditAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
