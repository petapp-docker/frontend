import { Component } from '@angular/core';
import {MatSidenavModule} from '@angular/material/sidenav';
import { AdminMenuComponent } from "../admin-menu/admin-menu.component";
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-admin',
    standalone: true,
    templateUrl: './admin.component.html',
    styleUrl: './admin.component.css',
    imports: [MatSidenavModule, AdminMenuComponent, RouterOutlet]
})
export class AdminComponent {

}
