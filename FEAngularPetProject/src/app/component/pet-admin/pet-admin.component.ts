import { FormsModule, NgForm } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { IPet } from '../../../../models/IPet';
import { PetService } from '../../services/pet.service';
import { IUser } from '../../../../models/IUser';
import { UserService } from '../../services/user.service';
import { RouterModule } from '@angular/router';
//import { IPetWithUser } from '../../../../models/IPetWithUser';
import { UiService } from '../../services/ui.service';
import { Observable, Subscription } from 'rxjs';
import { IFile } from '../../../../models/IFile';
import { PictureService } from '../../services/picture.service';
import { HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-pet-admin',
  standalone: true,
  templateUrl: './pet-admin.component.html',
  styleUrl: './pet-admin.component.css',
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
  ]
})
export class PetAdminComponent {
  user: IUser = {
    id: 0,
    username: ''
  };
  pet: IPet = {
    name:'',
    petType:'',
  }
  errorMessage: string = '';
  fileSizeError: string ='';
  pets: Array<IPet> = [];
  users: Array<IUser> = [];
  showAddPet: boolean = false;
  subscription!: Subscription;
  selectedUser = {
    id: 0,
    username: '',
  };
  petsAdminUrl: string = 'admins/pets';
  usersAdminUrl: string = 'admins/users/dto';
  petsWithUser: Array<IPet> = []; //22.02
  @Output() btnClick = new EventEmitter;

  //For PetPicture:
  msg: string = '';
  preview: any = '';
  currentFile?: File;
  idString: string = '';

  imageInfo?: IFile = {
    id: 0,
    imgName: '',
    url: '',
    type: '',
    imgData: '',
    date: ''
  };
  pictureName: string | undefined;
  fileSize:number = 65535;

  @ViewChild('petAdminForm') private petAdminForm!: NgForm;
  constructor(private petService: PetService,
    private userService: UserService,
    private uiService: UiService,
    private pictureService: PictureService) {

    this.subscription = this.uiService
      .onToggle()
      .subscribe((value) => (this.showAddPet = value));

  }

  ngOnInit(): void {
    this.getPets();
    this.getUsers();
  }

  // as I send PetWithUserDTO from BE, where is Pet and username, 
  // then the FE should accept that object, 
  // so IPetWithUser type of obj. is created
  getPets(): void {
    this.petService.getPets(this.petsAdminUrl).subscribe(
      value => {
        //console.log("THIS.PETSWITHUSER " + value[0].username);
        this.petsWithUser = value;
      },
    );
  }

  onDelete(petWithUser: IPet): void {

    this.petService.deletePet(this.petsAdminUrl, petWithUser).subscribe({
      next: () => this.petsWithUser = this.petsWithUser.filter(p => p !== petWithUser),
      error: (err) => {
        console.log(err);
        this.errorMessage = "You are not authorized to delete pets data!";
      }
    });
  }


  onAdd(name: string, petType: string) {
    name = name.trim();
    petType = petType.trim();


    if (this.petAdminForm.valid && this.currentFile === undefined) {
      // add the pet and connect it to the user.
      // User is of type IUser (from IPet), and if you change "user:" here, 
      // you should change this name in BE as well
      this.makePetServiceCall(name, petType, 0);

    } else if (this.petAdminForm.valid) {
      this.upload().subscribe({
        next: (imageId: string) => {
          let imageIdNum: number = parseInt(imageId);
          this.makePetServiceCall(name, petType, imageIdNum);
        },
        error: () => this.errorMessage = "Error adding pet!",
      });
    } else {
      console.log("Error in adding data to table!");
    }
  }

  makePetServiceCall(name: string, petType: string, imageIdNum: number) {
    // add the pet and connect it to the user.
    // User is of type IUser (from IPet), and if you change "user:" here, 
    // you should change this name in BE as well

    const user = this.getOwnerForPet(); //22.02

    //console.log("This is the user + onAdd" + user.username)
    this.petService.addPet(this.petsAdminUrl, { name, petType, user } as IPet, imageIdNum)
      .subscribe({
        next: () => {
          this.petService.getPets(this.petsAdminUrl).subscribe({
            next: (result: IPet[]) => this.petsWithUser = result,
            error: (err) => console.log("THE GET ERROR HERE: + " + err),
          });
        },
        error: (err) => {
          console.log(err);
          this.errorMessage = "Error! Pet not added - owner not selected.";
          console.log(this.errorMessage);
        }
      });
  }


  // gets users by UserService for the "Add Pet" form
  getUsers(): IUser[] {
    this.userService.getUsers(this.usersAdminUrl).subscribe(
      value => {
        this.users = value;
      },
    );
    return this.users;
  }


  getOwnerForPet(): any {
    // object retrieved because of <select ... [(ngModel)]="selectedUser"> and 
    // <option [ngValue]="thisSelectedUser" *ngFor="let thisSelectedUser of users">
    return this.selectedUser;
  }


  toggleAddPet(): void {
    this.uiService.toggleAddPet();
  }

  selectFile(event: any): void {
    this.msg = '';
    this.preview = '';
    const selectedFiles = event.target.files;
    if (selectedFiles) {
      const file: File | null = selectedFiles.item(0);

      if (file) {
        this.preview = '';
        this.currentFile = file;
        const reader = new FileReader();

        
        reader.onload = (img: any) => {
          //console.log(img.target.result)
          this.previewFile();

        };
        //Check file size before displaying
        this.fileSizeError = '';
        if(!this.checkFileSize(this.currentFile)){
          this.currentFile = undefined;
          return;
        }
        reader.readAsDataURL(this.currentFile);
      }
    }
  }

  // filesize is restricted by MySQL blob size.
  // If you want to change filesize restrictions,
  // see Spring BE PetProject entity package 
  // and ImageData.java - should change this part:
  // @Column(length=1000) private byte[] imageData
  checkFileSize(file:File):boolean{
    if(file.size > this.fileSize){
      this.fileSizeError = "Image is too big. Maximum filesize up to " +   
                            this.fileSizeConverter(this.fileSize) + 
                            "kB";
      return false;
    }
    return true;
  }

  fileSizeConverter(bytes: any){
    return Math.round(bytes / 1024);
  }

  //Idea received from: https://www.bezkoder.com/angular-17-file-upload/
  upload(): Observable<string> {
    return new Observable<string>((observer) => {
      if (this.currentFile) {
        this.pictureService.upload(this.currentFile).subscribe({
          next: (event: any) => {
            if (event instanceof HttpResponse) {
              this.msg = event.body.message;
              this.idString = event.body.parameterString;
              observer.next(this.idString);
              observer.complete();
              this.preview = '';
            }
          },
          error: (err: any) => {
            console.log(err);
            observer.error(err);

            if (err.error && err.error.message) {
              this.msg = err.error.message;
            } else {
              this.msg = 'Could not upload the file!';
              this.errorMessage = this.msg;
            }
          },
          complete: () => {
            this.currentFile = undefined;

          },
        });
      } else {
        this.currentFile = undefined;
        observer.error("No upload file selected!");
        observer.next(this.idString);
      }
    })
  }

  //This is working
  previewFile(): void {
    const reader = new FileReader();
    reader.onload = (img: any) => {
      this.preview = img.target.result;
      //console.log("IMG: " + this.preview);
    };
    reader.readAsDataURL(this.currentFile!);
  }
}
