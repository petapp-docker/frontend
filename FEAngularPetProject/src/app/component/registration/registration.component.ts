import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { IUser } from '../../../../models/IUser';

@Component({
  selector: 'app-registration',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './registration.component.html',
  styleUrl: './registration.component.css'
})
export class RegistrationComponent {
 
  user : IUser = {
    id : 0,
    username :'',
    password : ''
  }
  message: string ='';
  username: any;
  password: any;
  
  
  constructor(private authService: AuthService, private router: Router,) {}

  ngOnInit(): void {}
  
  // to register a new user:
  onSubmit(regForm: NgForm): void {
    
    this.authService.register(regForm.value.username, regForm.value.password).subscribe({
      next: (response) => {
        this.router.navigate(['/login'], { state: { message: 'You can now login!' } }),
        console.log('Registration success!'); 
      },
      error: (error) => {
        this.handleError(error);
      },
    });
    this.message ='';
  }

  // error.error.trace gets the error message text from BE 
  // method: registerUser (from AuthenticationController.java)
  handleError(error: HttpErrorResponse) {
    if (error.error.trace.includes("Duplicate entry")) {
      this.message = "Error: Username is taken!";
      console.log("Error description: " + error.error.trace);
    } else {
      console.log('Err: ' + JSON.stringify(error));
    }
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}
