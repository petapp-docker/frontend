import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PetService } from '../../services/pet.service';
import { Location } from '@angular/common';
import { IPet } from '../../../../models/IPet';


@Component({
  selector: 'app-pet-edit',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './pet-edit.component.html',
  styleUrl: './pet-edit.component.css'
})
export class PetEditComponent {
  // pass this url ending to pet.service when subscring
  // in order to use pet.service from multiple components
  // which need to access different urls:
  petsUrl: string = 'pets';
  errorMessage: string ='';
  pet: IPet = {
    id: 0,
    name: '',
    petType : '',
  };

  constructor(
    private route:ActivatedRoute,
    private petService:PetService,
    private location: Location
  ){} 

  ngOnInit(): void {
    this.getPet();
  }

  getPet(): void {
    // Obtaining the value of "id" parameter and converting to number
    // +this... (unary plus -- trying to convert value to number)
    const id = +this.route.snapshot.paramMap.get('id')!;

    //get pet info:
    this.petService.getPet(this.petsUrl, id)
      .subscribe(pet => this.pet = pet);
  }

  //Navigates back in the browser URL history.
  goBack(): void {
    this.location.back();
  }

  //save and go back to previous page
  onSave(): void {
    this.petService.updatePet(this.petsUrl, this.pet)
    .subscribe(
      {
        next: () => this.goBack(),
        error: (err) => {
          this.errorMessage = "You are not authorized to edit pet details!";
        }
      }
    );
  }
}
